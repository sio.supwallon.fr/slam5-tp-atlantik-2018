﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlantik.Classes;
using Atlantik.Tools;
using MySql.Data.MySqlClient;

namespace Atlantik.Dao
{
    class BateauVoyageurDao : AbstractDao<BateauVoyageur>
    {
        public BateauVoyageurDao() : base() { }

        public BateauVoyageurDao(string path) : base(path) { }

        public BateauVoyageurDao(DatabaseConfiguration dbConfig) : base(dbConfig) { }

        public int CreateTable()
        {
            Console.WriteLine("CREATE TABLE `bateau_voyageur` (...);");

            int res;

            string cmdText = @"
                CREATE TABLE IF NOT EXISTS `bateau_voyageur`
                (
                    `id_bateau` INT NOT NULL,
                    `vitesse`   FLOAT NOT NULL,
                    `image`     VARCHAR(255) NOT NULL,
                    PRIMARY KEY (`id_bateau`),
                    FOREIGN KEY (`id_bateau`) REFERENCES `bateau`(`id`)
                )Engine=InnoDB;
            ";

            Open();
            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            res = cmd.ExecuteNonQuery();
            Close();

            return res;
        }

        public override int Create(BateauVoyageur item)
        {
            throw new NotImplementedException();
        }

        public override int Delete(BateauVoyageur item)
        {
            throw new NotImplementedException();
        }

        public override BateauVoyageur Read(int id)
        {
            throw new NotImplementedException();
        }

        public override List<BateauVoyageur> ReadAll()
        {
            string cmdText = @"
                SELECT *
                FROM `bateau_voyageur`;
            ";

            Open();

            MySqlCommand cmd = new MySqlCommand(cmdText, Connection);
            MySqlDataReader reader = cmd.ExecuteReader();

            List<BateauVoyageur> bateauxVoyageurs = new List<BateauVoyageur>();


            while (reader.Read())
            {
                int _id = reader.GetInt32("id_bateau");

                BateauDao bateauDao = new BateauDao(DatabaseConfiguration);
                Bateau bateau = bateauDao.Read(_id);

                EquipementDao equipementDao = new EquipementDao(DatabaseConfiguration);

                BateauVoyageur voyageur = new BateauVoyageur(bateau.Id, bateau.Nom, bateau.Longueur, bateau.Largeur);

                voyageur.Vitesse = reader.GetDecimal("vitesse");
                voyageur.Image = reader.GetString("image");
                voyageur.LesEquipements = equipementDao.ReadAllByIdBateau(bateau.Id);

                bateauxVoyageurs.Add(voyageur);
            }

            reader.Close();
            Close();

            return bateauxVoyageurs;
        }

        public override int Update(BateauVoyageur item)
        {
            throw new NotImplementedException();
        }
    }
}
