﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlantik.Dao
{
    interface IDao<T>
    {
        int Create(T item);
        T Read(int id);
        List<T> ReadAll();
        int Update(T item);
        int Delete(T item);
    }
}
