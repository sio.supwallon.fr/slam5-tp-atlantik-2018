﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlantik.Tools
{
    class PDF
    {
        /// <summary>
        /// Constructeur de la classe. Crée un document PDF vierge dont le chemin absolu est fourni dans path
        /// </summary>
        /// <param name="path"></param>
        public PDF(string path)
        {
            Console.WriteLine("Create file " + path);
        }

        /// <summary>
        /// Écrit le contenu de la chaîne de caractères text dans le document PDF
        /// </summary>
        /// <param name="text"></param>
        public void WriteLine(string text)
        {
            Console.WriteLine("Add text : " + text);
        }

        /// <summary>
        /// Insère dans le document l'image dont le chemin d'accès est passé en paramètre
        /// </summary>
        /// <param name="path"></param>
        public void LoadImageFromFile(string path)
        {
            Console.WriteLine("Load image from file : " + path);
        }

        /// <summary>
        /// Ferme le document.
        /// </summary>
        public void Close()
        {
            Console.WriteLine("Close file");
        }
    }
}
