﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlantik.Classes
{
    class Bateau
    {
        // Propriété(s)

        public int Id { get; set; }
        public string Nom { get; set; }
        public decimal Longueur { get; set; }
        public decimal Largeur { get; set; }

        // Constructeur(s)

        public Bateau() { }

        public Bateau(int id, string nom, decimal longueur, decimal largeur)
        {
            Id = id;
            Nom = nom;
            Longueur = longueur;
            Largeur = largeur;
        }

        // Méthode(s)

        /// <summary>
        /// Retourne sous la forme d'une chaîne de caractères toutes les valeurs concaténées 
        /// des attributs de la classe précédées de leurs libellés.
        /// Exemple : Nom du bateau : Luce isle
        ///           Longueur : 37,20 mètres
        ///           Largeur  : 8,60 mètres
        /// </summary>
        public override string ToString()
        {
            string str = "";

            str += "Nom du bateau : " + Nom + Environment.NewLine;
            str += "Longueur : " + Longueur + " mètres" + Environment.NewLine;
            str += "Largeur  : " + Largeur + " mètres" + Environment.NewLine;

            return str;
        }
    }
}
