﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Atlantik.Classes
{
    class BateauFret : Bateau
    {
        // Propriété(s)

        public int PoidsMax { get; private set; }

        // Constructeur(s)
        public BateauFret() { }

        public BateauFret(int id, string nom, decimal longueur, decimal largeur)
            : base(id, nom, longueur, largeur) { }

        public BateauFret(int id, string nom, decimal longueur, decimal largeur, int poidsMax)
            : base(id, nom, longueur, largeur)
        {
            PoidsMax = poidsMax;
        }

        // Méthode(s)

        /// <summary>
        /// Retourne sous la forme d'une chaîne toutes les valeurs concaténées des attributs de la
        /// classe, sauf l'attribut imageBatVoy qui n'est pas inséré dans la chaîne concaténée.
        /// Chaque valeur est précédée de son libellé.
        /// Exemple : Nom du bateau : Luce isle
        ///           Longueur : 37,20 mètres
        ///           Largeur  : 8,60 mètres
        ///           Poids Max: 20 000 kg
        /// Remarque :
        /// On utilisera l’opérateur "+" pour concaténer des valeurs de type string et 
        /// la propriété de classe Environment.NewLine pour marquer une fin de ligne.
        /// Exemple : str = "Liste des équipements du bateau : " + Environment.NewLine;
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            string str = base.ToString();

            str += "Poids Max: " + PoidsMax + " kg" + Environment.NewLine;

            return base.ToString();
        }
    }
}
